#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include "chararrutil.h"

using namespace std;

auto * bmp_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image) {

    auto * results = new list< pair<unsigned long, unsigned long> >;
    if(!image){printf("(!image) is true! Fix that!\n"); return results;}// remove in release
    
    for (auto t : *found){
		if (t.second != T_BMP_HEAD){continue;}
		
		image.seekg(t.first, image.beg);
		
		char bmpfile_header[16]; // 14
		image.read(bmpfile_header, 14);
		if (!image){return results;} // Wenn ein Fehler beim lesen auftrat ist vermutlich Dateiende erreicht => kann kein BMP sein
		
		uint32_t pixeldata_offset = getuint(bmpfile_header + 0xa, 4);
		
		if (pixeldata_offset < 54 || pixeldata_offset > 8*1024){ // Daten wuerden im Header anfangen / haeder waere riessig
			continue;
		}		

		char bmpinfo_header[48]; //40
		image.read(bmpinfo_header, 40);
		if (!image){return results;}
						
		if (getuint(bmpinfo_header, 4) < 40){ // meist 40. bei ffmpeg encodeten Images meisst groesser
			continue;
		}

		int b = getuint(bmpinfo_header + 4, 4);
		int h = (uint32_t)getuint(bmpinfo_header + 8, 4);
		h = (h>0) ? h : -h;
		int depth = getuint(bmpinfo_header + 14, 2);

		switch(depth){
			default: continue; // ungueltige Farbtiefe
			case 1:	case 4:	case 8:	case 16: case 24: case 32: ;
		}

        int compression = (uint32_t)getuint(bmpinfo_header + 16, 4);

        // formula stolen from https://en.wikipedia.org/wiki/BMP_file_format
        uint32_t rowlength=(depth*b+31)/32;
		
		uint32_t datasize = getuint(bmpinfo_header+20,4) ?: rowlength*h;
        
        // Breite muss mit 0-Bytes auf durch 4teilbare Breite gepaddet werden.
        
        int valid_bmp = 1;
        char paddingarr[4];
        int numofpaddingperline = 0;
        if (( numofpaddingperline = ((4-(b % 4))&3) )){
            if ((!compression)){
                for (int i = 0; i < h; i++){
                    image.seekg(t.first + pixeldata_offset + i*rowlength + b, image.beg);
                    image.read(paddingarr, numofpaddingperline);
                    if (!image){valid_bmp = 0; break;}
                    if (getuint(paddingarr, numofpaddingperline)){valid_bmp = 0; break;} // padding != 0
//                  printf("Paddingtest for Pixelline %d successuf\n", i);
                }
            } else {
//               printf("compression %d\n", compression);
                if(compression == 3){
                    // laut Wikipedia ist dies nur erlaubt bei den Bittiefen 16 und 32
                    if (depth != 16 && depth != 32){
                        valid_bmp = 0;
                    }
                }      
            }
        }

        if (!valid_bmp){
            continue;
        }

//      printf("ValidBMP-File: Size: %d*%d Farbtiefe: %d Databereichgroesse: %d\n", b,h,depth, datasize);		
		results->push_back({t.first, t.first+pixeldata_offset+datasize});
	    	
    }
    
//   for (auto e: *results){
//		printf("Positionen %ld bis %ld\n", e.first, e.second);
//	}
    
    return results;
}

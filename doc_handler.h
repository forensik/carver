#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>


using namespace std;
//OLESS-Header according to:
//	http://download.microsoft.com/download/9/5/E/95EF66AF-9026-4BB0-A41D-A4F81802D92C/%5BMS-CFB%5D.pdf
struct OLESS_header{
	char sig[8];
	char clsid[16];
	uint16_t verMinor;
	uint16_t verMajor;
	uint16_t byteOrder;
	uint16_t sectorShift;
	uint16_t miniShift;
	uint16_t reserved;
	uint32_t reserved2;
	uint32_t numDirSects;
	uint32_t numFatSects;
	uint32_t dirFirstSect;
	uint32_t transactSig;
	uint32_t miniStrMax;
	uint32_t miniFatFirstSect;
	uint32_t numMiniFatSects;
	uint32_t diFatFirstSect;
	uint32_t numDiFatSects;
	uint32_t diFat[109];
}__attribute__((packed));

unsigned int getFilesize(struct OLESS_header* hdr, unsigned int headerLoc, ifstream& image){
	unsigned int sectorSize = 0;
	if(hdr->sectorShift == 9 and hdr->verMajor == 3){
		sectorSize = 512;
	}else if(hdr->sectorShift == 12 and hdr->verMajor == 4){
		sectorSize = 4096;
	}
	uint32_t currentFatSect[4096 / 4]; // sectorSize / 4 brauchen wir leider, weil die je nach wordversion und anderen parametern variieren kann. Eine konstante 4096 fuehrt sicher zu Fehlern v.a. bei aelternen .docs
    // wenn man hier sectorSize durch 4096 ersetzt, veraendert man nicht die sectorsize
    // (Sie ist weiterhin 512 oder 4096 oder 0). Dan einzigste was man aendert ist die groesse des Puffers.
    // Man verwendet somit bei kleineren Sektorgroessen nur einen Teil des Puffers,
    // waehrend der Rest einfach ignoriert wird.
    // Da die Warnung "taking address of array of runtime bound [-Wvla]"
    // jedoch nur beim aelteren gcc bei -Wall auftritt(Debian 4.9.2) und bei neueren(5.1.0) nicht mehr auftritt,
    // ist sectorsize/4 hier auch eine akzeptable Loesung.
    //
    //  Interessanterweise produziert ein -Wvla noch weitere Probleme in 5.1.0, die jedoch scheinbar zur
    //  compilezeit statisch festgelegt werden koennen....  
	//
	//  stimmt da hast du allerdings recht. Hatte nicht beachtet, dass wir ja nur noch in den puffer
	//  schreiben und die Groesse nie wieder lesend verwenden.

 
	unsigned int sizeInSectors = 1;
	for(unsigned int sectorNumber = 0; sectorNumber < hdr->numFatSects; sectorNumber++){
		image.seekg(headerLoc+(sectorSize*((hdr->diFat[sectorNumber])+1)), image.beg);
		image.read((char*)&currentFatSect, sectorSize);
		for(unsigned int i = 0; i < sectorSize / 4; i++){
			if(currentFatSect[i] != 0xFFFFFFFF){sizeInSectors++;}
		}
	}
	return sizeInSectors * sectorSize;
}

auto * doc_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image){
	char zeroedClsID[] = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
	auto * results = new list< pair<unsigned long, unsigned long> >;
	if(!image){return results;}
	

	for(auto t : *found){
		image.seekg(t.first, image.beg);
		struct OLESS_header hdr;
		image.read((char*)&hdr, sizeof(hdr));
		if(strncmp(hdr.sig, S_DOC_HEAD, 6) != 0){continue;}

		
		//clsid must be all 0x00
		if(memcmp(hdr.clsid, zeroedClsID, 16)){
			continue;
		}

		//major version must be either 0x03 or 0x04
		if(!((hdr.verMajor == 3) || (hdr.verMajor == 4))){
			continue;
		}
		
		//byte order MUST be little endian. This is marked with 0xFFFE
		if(hdr.byteOrder != 0xFFFE){
			continue;
		}
		
		//sector shift MUST be either 9 or 12
		if(!((hdr.sectorShift == 9) || (hdr.sectorShift == 12))){
			continue;
		}

		//mini-sector shift MUST be 6
		if(hdr.miniShift != 6){
			continue;
		}

		//the reserved fields must be all 0x00
		if(hdr.reserved != 0 || hdr.reserved2 != 0){continue;}

		//if major-version is 3 then numDirSects must be 0
		if(hdr.verMajor == 3 and hdr.numDirSects != 0){continue;}

		if(hdr.miniStrMax != 0x1000){continue;}


		//all the constraints from OLESS-documentation were checked
		//now we can start parsing the header and get the size of the .doc file
		unsigned int docSize = getFilesize(&hdr, t.first, image);

		//@debug
		//cout << "docSize in Bytes: " << docSize << endl;
		
		results->push_back({t.first, t.first+docSize}); // Anfang und Ende

	}
	image.seekg(0, image.beg);
	image.clear();
	return results;
}



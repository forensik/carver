#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;

void print_found_tokens(list< pair<unsigned long, int> > * found){
    for(auto p : *found){
        cout << "0x" << hex << p.first << dec<<"\t" << debug_print_token(p.second) << endl;
    }
}

bool jpg_analyse_header(unsigned long start_pos, ifstream& image){
	image.seekg(start_pos, image.beg);
    while(1){
    //	image.ignore(/*std::numeric_limits<std::streamsize>::max()*/ 1, 0xff);
        char marker = image.get();
        if (!image.good()){
            image.clear();
            return false;
        }
        if(marker != '\xff'){
            //cout << "0xff not found; got " << hex << ((uint8_t)marker & 0xff) << " at " << image.tellg() << endl;
            return false;
        }
        marker = image.get();
        if (!image.good()) {
            image.clear();
            return false;
        }
        //cout << hex << (((int)marker)&0xFF) << endl;
        if (marker == '\xd9'){
            //footer, too short
            return false;
        }
        if (marker == '\xda'){
            //start of scan
            //=> header seems to be ok.
            //we could check for more until the footer, but things get complicated
            return true;
        }
        if (!((marker == '\x01') || ((marker >= '\xd0') && (marker <= '\xd8')))){
            //marker is followed by a two byte size
            unsigned short size = ((image.get() & 0x7f) <<8); 
            if (!image.good()){
                image.clear();
                return false;
            }
            size |= (image.get() & 0xff);
            if (!image.good()){
                image.clear();
                return false;
            }
            image.seekg(size-2, image.cur);
//            cout << "size: " << hex << size << endl;
        }else{
//            cout << "no size" << endl;
            //stand alone markers, next marker must follow immediately
        }
        

        if(!image){
            cout << "image bad ";
            image.clear();
            return false;
        }

    }
    //return true;
}

auto * jpg_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image) {
    list< pair<unsigned long, int> > jpg_list;
    auto * results = new list< pair<unsigned long, unsigned long> >;
    //remove duplicate jpg headers
    for (auto it = found->begin(); it != found->end(); ++it){
        switch (it->second){
            case T_JPG_HEAD_3: 
                if ((jpg_list.back().second == T_JPG_HEAD_1) || (jpg_list.back().second == T_JPG_HEAD_2)){
                    if (jpg_list.back().first == it->first){
                        //ingore duplicate jpg headers
                        continue;
                    }
                }
            case T_JPG_HEAD_1:	
            case T_JPG_HEAD_2:	
                //analyse: if header is valid
                if(jpg_analyse_header(it->first, image)){
                    //cout<<hex << it->first << " correct" << endl;
                } else {
                    //cout<<hex << it->first << " bad!" << endl;
                    continue;
                }
                jpg_list.push_back(*it); 
                break;
            case T_JPG_FOOT:
                if (jpg_list.empty()){
                    continue;
                } else {
                    results->push_back({jpg_list.back().first, it->first+L_JPG_FOOT});
                    jpg_list.pop_back();
                    break;
                }
            default:
                continue;
        }
    }
    
//    cout << "jpgs" << endl;
//    print_found_tokens(&results);
    return results;
}





/*
Aufbau JPG:
Marker 0xff ?   (? != 0xff, 0x0)
    startet ein Segment. 

*/


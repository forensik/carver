#pragma once

void dump_hex(char * addr, int len){
	for (int i=0; i < len; i++){
		printf("%02X ", 0xff & addr[i]);
	}
	printf("\n");
}

uint64_t getuint(char * addr, unsigned char len){
	uint64_t result = 0;
	for (int i = 0; i < len; i++){
		result |= ((unsigned char)addr[i])<<(i<<3);
	}
	return result;
}

uint64_t getuint_2(char * addr, unsigned char len){
	uint64_t result = 0;
	for (int c=0; c<len; c++){
        result <<= 8;
        result |= (unsigned char)addr[c];
    }
    return result;
}

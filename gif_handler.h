#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include "chararrutil.h"

using namespace std;

auto * gif_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image) {
   
    auto * results = new list< pair<unsigned long, unsigned long> >;
    
    if(!image){printf("(!image) is true! Fix that!\n"); return results;}// remove in release
    
    auto * gifends = new list< unsigned long >;
    auto * gifbegs = new list< unsigned long >;    

    for (auto t : *found){
		if ((t.second == T_GIF_HEAD_1) || 
            (t.second == T_GIF_HEAD_2)) {
                gifbegs->push_back(t.first);    
                continue;}
        if ((t.second == T_GIF_FOOT_1)){
//            (t.second == T_GIF_FOOT_2)) {
                gifends->push_back(t.first);
                continue;}
        /* unreachable position ...*/
    }


    for (auto t : *gifbegs){

            image.seekg(t+6, image.beg); // header ueberspulen
            char screen_desc[8];
            image.read(screen_desc, 4);
            if (!image){continue;}
            
            int s_width = getuint(screen_desc, 2);
            int s_height = getuint(screen_desc+2, 2);

            if (s_width > 12000 || s_height > 12000){ // jaja laut Standard koennen die auch groesser sein...
                continue;
            }

            

//            printf("--> %d, %d\n", s_width, s_height);


            
            for (auto e : *gifends){
                if (e <= t+50){ // Ende muss mind 50 Byte nach Anfang liegen (unser Test gif ist 78 Byte(!) gross)


                    continue;
                }
                if ((e-t) > 4*1024*1024){
                    continue; // Max 4 MB
                }    
        
                if ((e-t) < (s_width * s_height) * .4 ) { // kleiner als 0.4 * Pixelanzahl
                    continue;
                }
                
                if (((int)((e-t) + 300 ))< (int)((s_width * s_height) * .8)) { // +300 immer noch zu klein 
                    continue;
                }

                if ((int) ((e-t) - 120) > (int)((s_width * s_height) * 1.4)) { // -120 immernoch zu groess ...
                    continue;
                }
                
                results->push_back({t, e+2});
                break;
                
            }
                

    }   
	image.seekg(0, image.beg);
	image.clear();
 
    return results;
}

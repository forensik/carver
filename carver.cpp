#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <utility>
#include "headers.h"
#include <forward_list>
#include <list>
#include <string>
#include <string.h>
#include <strings.h>
#include <cassert>
#include <sys/stat.h>

#include "jpg_handler.h"
#include "bmp_handler.h"
#include "gif_handler.h"
#include "png_handler.h"
#include "doc_handler.h"
#include "pdf_handler.h"

using namespace std;

#define K 1024
#define BUFLEN 100 * K * K
#define OVERLAP 12

vector <pair<string, int > > tokens = {
    {{S_JPG_HEAD_1, L_JPG_HEAD_1},  T_JPG_HEAD_1}, 
    {{S_JPG_HEAD_2, L_JPG_HEAD_2},  T_JPG_HEAD_2}, 
    {{S_JPG_HEAD_3, L_JPG_HEAD_3},  T_JPG_HEAD_3},
	{{S_JPG_FOOT,	L_JPG_FOOT},    T_JPG_FOOT	},	
            
	{{S_GIF_HEAD_1,	L_GIF_HEAD_1},	T_GIF_HEAD_1},
	{{S_GIF_HEAD_2,	L_GIF_HEAD_2},	T_GIF_HEAD_2},
	{{S_GIF_FOOT_1,	L_GIF_FOOT_1},	T_GIF_FOOT_1},
//	{{S_GIF_FOOT_2,	L_GIF_FOOT_2},	T_GIF_FOOT_2},
            
	{{S_PNG_HEAD,	L_PNG_HEAD},	T_PNG_HEAD	},
//	{{S_PNG_FOOT,	L_PNG_FOOT},	T_PNG_FOOT	},
            
	{{S_BMP_HEAD,	L_BMP_HEAD},	T_BMP_HEAD	},
            
	{{S_DOC_HEAD,	L_DOC_HEAD},	T_DOC_HEAD	},
            
	{{S_PDF_HEAD,	L_PDF_HEAD},	T_PDF_HEAD	},
	{{S_PDF_FOOT,	L_PDF_FOOT},	T_PDF_FOOT	}

    };


     


void fail(){
    cerr << "\033[31mFehler sind aufgetreten. Nichts wurde rausgeschrieben.\nChecke vorherige Ausgaben fuer genauere Informationen.\033[30m" << endl;
    exit(1);
}

      
auto* parse(ifstream& image){
    /*
        parse image, search for tokens (from global vector tokens)
        returns list<pair(POS,TOKEN)>
        POS: byte-position of string of TOKEN in file
    */
    auto * results = new forward_list<pair<unsigned long, int> > ;

	unsigned int fileSize = 0;
	image.seekg(0, image.end);
	fileSize = image.tellg();

	image.seekg(0, image.beg); // An Anfang der Datei spulen
 
    char* buf = new char[BUFLEN];
    bzero(buf, BUFLEN);
    image.read(buf, BUFLEN);
    
/*
        |<-----BUFLEN----->|
        |              |<-----BUFLEN----->|
        |              |<->|          |<-----BUFLEN--
        |                ^
        |                Overlap
        |
        |              |              |
abspos: 0              0+B.LEN-O.Lap  (B.Len-O.Lap)*2   
*/

    void * pos;
    unsigned int chunkNo = 0;
    /*
        Die folgende Schleife schaut rel. kompliziert aus, funktioniert aber und ist schnell
        Sie hat eine eigene Dokumentation in searcher.{svg,pdf}
    */
    for (unsigned long abspos = 0;; abspos += BUFLEN-OVERLAP){
	    //cout << "digging through chunkNo: " << chunkNo++ << endl;
    	cout << "finding file headers and footers: " << fixed << setprecision(2) << (100*(chunkNo++*(BUFLEN-OVERLAP)/double(fileSize)))  << "%\r";
	    cout.flush();
        for (auto e: tokens){
            for (int offset=0;
             (pos = memmem(buf+offset, !!image ? BUFLEN-offset : image.gcount()+OVERLAP-offset, e.first.c_str(), e.first.size()))
             != NULL;){
                if (!image || (((unsigned long) pos - (unsigned long)buf) < BUFLEN-OVERLAP)){
                    results->push_front({((offset = (unsigned long)pos-(unsigned long)buf) + abspos), e.second});
                    offset ++;
                } else break;
            }
        }
        if (!image) break;
        memcpy(buf, buf+BUFLEN-OVERLAP, OVERLAP);
        image.read(buf + OVERLAP, BUFLEN-OVERLAP);
    }
	cout << "finding file headers and footers: " << fixed << setprecision(2) << (100.00)  << "%" << endl;
    delete[] buf; 
    return results;
}

auto * filter_found_tokens(forward_list< pair<unsigned long, int> > * found){
    //copies list, removes everythin we can't use anymore

    auto * results_1 = new forward_list<pair<unsigned long, int> > ;

    int jpg_headers = 0;
    int gif_headers = 0;
    int png_headers = 0;
    int bmp_headers = 0;
    int doc_headers = 0;
    int pdf_headers = 0;

    int jpg_footers = 0;
    int gif_footers = 0;
//    int png_footers = 0;
//    int bmp_footers = 0;
//    int doc_footers = 0;
    int pdf_footers = 0;


    for(auto t : *found){
    /*
        remove footers until first header is found
    */
        switch(t.second){
            case T_JPG_HEAD_1:	
            case T_JPG_HEAD_2:	
            case T_JPG_HEAD_3: 
                jpg_headers++;
                break;
            case T_JPG_FOOT:	
                if(jpg_headers == 0){
                    //remove footer
                    continue;
                }	
                jpg_footers++;
                break;
            case T_GIF_HEAD_1:	gif_headers++; break;	
            case T_GIF_HEAD_2:	gif_headers++; break;	
            case T_GIF_FOOT_1:		
                if(gif_headers == 0){
                    //remove footer
                    continue;
                }	
                gif_footers++;
                break;
//            case T_GIF_FOOT_2:			
//                if(gif_headers == 0){
//                    //remove footer
//                    continue;
//                }	
//                gif_footers++;
//                break;
	
            case T_PNG_HEAD:	png_headers++; break;	
//            case T_PNG_FOOT:			
//                if(png_headers == 0){
//                    //remove footer
//                    continue;
//                }	
//                png_footers++;
//                break;
	
            case T_BMP_HEAD:	bmp_headers++; break;	
            case T_DOC_HEAD:	doc_headers++; break;	
            case T_PDF_HEAD:	pdf_headers++; break;	
            case T_PDF_FOOT:				
                if(pdf_headers == 0){
                    //remove footer
                    continue;
                }	
                pdf_footers++;
                break;
            default:
                cout << "error: unknown token" << endl;
                assert(false);


        }
        //token survived, so we can copy it
        results_1->push_front(t);
    }

    jpg_headers = 0;
    gif_headers = 0;
    //png_headers = 0;
    pdf_headers = 0;
                    
    jpg_footers = 0;
    gif_footers = 0;
    //png_footers = 0;
    pdf_footers = 0;

    auto * results_2 = new list<pair<unsigned long, int> > ;
    for(auto t : *results_1){
    /*
        reversed order of found
        remove headers from back until first footer is found (only types with footers)
    */
        switch(t.second){
            case T_JPG_HEAD_1:	
            case T_JPG_HEAD_2:	
            case T_JPG_HEAD_3:  
                if(jpg_footers == 0){
                    continue;
                }
                jpg_headers++; 
                break;	
            case T_JPG_FOOT:    jpg_footers++; break;
            case T_GIF_HEAD_1:	
            case T_GIF_HEAD_2:	
                if(gif_footers == 0){
                    continue;
                }
                gif_headers++; 
                break;	
            case T_GIF_FOOT_1:	gif_footers++; break;
//            case T_GIF_FOOT_2:  gif_footers++; break;
//            case T_PNG_HEAD:	
//                if(png_footers == 0){
//                    continue;
//                }
//                png_headers++; 
//                break;	
//            case T_PNG_FOOT:	png_footers++; break;
            case T_PDF_HEAD:
	            if(pdf_footers == 0){
                    continue;
                }
                pdf_headers++; 
                break;	
            case T_PDF_FOOT:	pdf_footers++; break;
            default: break;
        }

        //token survived, so we can copy it
        results_2->push_back(t);

    }

    //statistic for debugging:
    cout << "jpg_headers:	" << jpg_headers <<endl;
    cout << "jpg_footers:	" << jpg_footers <<endl;
    cout << "gif_headers:	" << gif_headers <<endl;
    cout << "gif_footers:	" << gif_footers <<endl;
    cout << "png_headers:	" << png_headers <<endl;
    cout << "bmp_headers:	" << bmp_headers <<endl;
    cout << "doc_headers:	" << doc_headers <<endl;
    cout << "pdf_headers:	" << pdf_headers <<endl;
    cout << "pdf_footers:	" << pdf_footers <<endl;

    delete results_1;
    return results_2;

}




void print_results (list <pair<unsigned long, unsigned long> > * found){
    for(auto p : *found){
        cout << "0x" << hex << p.first << "-0x" << p.second << dec << endl;
    }
}


void print_found_tokens(forward_list< pair<unsigned long, int> > * found){
    for(auto p : *found){
        cout << "0x" << hex << p.first << dec<<"\t" << debug_print_token(p.second) << endl;
    }
}

int create_folders(string folder){
    int failure = 0;
    vector <string> exts = {"jpg", "bmp", "pdf", "png", "gif", "doc" };

    // TODO mit tosearch synchronisieren!

    mkdir(folder.c_str(),  S_IRWXU | S_IRWXG);

    for (auto f: exts){

        string foldername(folder);
        foldername += "/";
        foldername += f.c_str(); 

        printf("foldername: %s\n", foldername.c_str());

        if (mkdir(foldername.c_str(), S_IRWXU | S_IRWXG)){
            printf("Fail @ outputfoldercreation: [errno=%d] -- %s\n", errno, strerror(errno));
            failure = 1;
        }
    }
    return failure;
}

void write_out(string folder, string extension, list< pair<unsigned long, unsigned long> > * items, ifstream& file){


    for (auto e: * items){

        char filename[20];
        snprintf(filename, 20, "%08lX.", e.first);
        ofstream output (folder + "/" + extension + "/" + filename + extension, ofstream::out | ofstream::binary);
        unsigned int size = e.second - e.first;
        if (size > 40 * K * K){
            continue; // TODO irgendwie ohne malloc loesen...
        }
        file.clear();
        file.seekg(e.first, file.beg);
        char * data = (char*) malloc(size);
        file.read(data, size);
        output.write(data, size);
        free(data);
        output.close();
    }


}

int main(int argc, char** argv){


    /*

        Quelldatei
        -o Ausgabeverzeichnis - sonst output
        -t zu suchende DAteien
        -c Config

    */
    // Defaultvalues
    string outputfile = "./output";
    string tosearch = "jpg,gif,png,bmp,doc,pdf";
    string configstring = "";

    long usedargs_bitmap = 1;
    for (int counter = 1; counter < min(argc-1, 64); counter++){
        // nur duchiterieren, bis noch ein Element danach ist -> dies ist der Wert davon
       
        if (!strcasecmp("-o", argv[counter])){
            outputfile = argv[counter+1];
            usedargs_bitmap |= 3 << counter; // 11(bin) shiften, da man naechstes Argument ja auch noch mit verarbeitet
            counter ++;
        }

        if (!strcasecmp("-t", argv[counter])){
            tosearch = argv[counter+1];
            usedargs_bitmap |= 3 << counter;
            counter ++;
        }

        if (!strcasecmp("-c", argv[counter])){
            configstring = argv[counter+1];
            usedargs_bitmap |= 3 << counter;
            counter ++;
        }
    }

    string imagename;
    int unknown = 0;
    for (int counter = 0; counter < argc; counter++){
        if (!(usedargs_bitmap & (1 << counter))){
                imagename = argv[counter];
                unknown ++;
        }
    }
   
    printf("%20s: %s\n%20s: %s\n%20s: %s\n",
        "Outputfilename", outputfile.c_str(),    
        "Searchfiles", tosearch.c_str(),
        "Configfile", configstring.c_str() );

    if (unknown != 1){
        printf("Wrong Commandline! Following Arguments could be interpreted as Imagefilename:\n");
        for (int q = 0; q < argc; q++){
            if (!(usedargs_bitmap & 1 << q)) printf("%s ", argv[q]);
        }
        printf("\n");
        printf("Commandline: %s [-c config] [-t search] [-c config] Imagefilename\n", argv[0]);
        return -1;
    } else {
        printf("%20s: %s\n", "Imagedatei", imagename.c_str());
    }

    /*hier beginnt das eigentliche Programm
    -----------------------------------------------------------------------------
    */

    ifstream image (imagename, ifstream::in | ifstream::binary);
    if (!image){
			printf("unable to open file!\n");
	        fail();
	}

    auto * found = parse(image);   
    image.seekg(0, image.beg);
	image.clear();
    cout << "1. run complete!" << endl;

    cout << "sorting...";
    found->sort();
    cout << "done!" << endl;
//    cout << "found: " << endl;
//    print_found_tokens(found);
    cout << "filterting..." << endl;;
    auto * filtered = filter_found_tokens(found);
    cout << "done!" << endl;
    delete found;
    cout << "found and filtered: " << endl;
    //print_found_tokens(filtered);

    auto * filtered_jpg = new forward_list<pair<unsigned long, int> >;
    auto * filtered_bmp = new forward_list<pair<unsigned long, int> >;
    auto * filtered_gif = new forward_list<pair<unsigned long, int> >;
    auto * filtered_png = new forward_list<pair<unsigned long, int> >;
    auto * filtered_doc = new forward_list<pair<unsigned long, int> >;
    auto * filtered_pdf = new forward_list<pair<unsigned long, int> >;


    for(auto t : *filtered){
        switch(t.second){
            case T_JPG_HEAD_1:	
            case T_JPG_HEAD_2:	
            case T_JPG_HEAD_3: 
            case T_JPG_FOOT:	
                filtered_jpg->push_front(t); 
                break;
            case T_GIF_HEAD_1:		
            case T_GIF_HEAD_2:		
            case T_GIF_FOOT_1:		
//            case T_GIF_FOOT_2:			
                filtered_gif->push_front(t); 
                break;
            case T_PNG_HEAD:
                filtered_png->push_front(t); 
                break;
            case T_BMP_HEAD:	
                filtered_bmp->push_front(t); 
                break;	
            case T_DOC_HEAD:
                filtered_doc->push_front(t); 
                break;	
            case T_PDF_HEAD:
            case T_PDF_FOOT:				
                filtered_pdf->push_front(t); 
                break;
            default:
                cout << "error: unknown token" << endl;
                assert(false);


        }
    }


	auto * jpgs = jpg_handler(filtered_jpg, image);
	auto * bmps = bmp_handler(filtered_bmp, image);
	auto * gifs = gif_handler(filtered_gif, image);
	auto * pngs = png_handler(filtered_png, image);
	auto * docs = doc_handler(filtered_doc, image);
	auto * pdfs = pdf_handler(filtered_pdf, image);

    if(create_folders(outputfile)){
        // LOL mit Hintergrundfarbe(41 statt 31 und 40 statt 30 fuern reset) is  des is buggy in urxvt... bis man die groesse aendert, dann gehts, aber in rxvt gehts sowieso
        fail();
    }

	printf("JPGS:\n"); print_results(jpgs);
	write_out(outputfile, string("jpg"), jpgs, image);
	printf("BITMAMPS:\n"); print_results(bmps);
	write_out(outputfile, string("bmp"), bmps, image);
	printf("GIFS:\n"); print_results(gifs);
	write_out(outputfile, string("gif"), gifs, image);
	printf("PNGS:\n"); print_results(pngs);
	write_out(outputfile, string("png"), pngs, image);
	printf("DOCS:\n"); print_results(docs);
	write_out(outputfile, string("doc"), docs, image);
	printf("PDFS:\n"); print_results(pdfs);
	write_out(outputfile, string("pdf"), pdfs, image);
	
    delete filtered;
    image.close();
    return 0;

}



CCFLAGS+=-O3 -Wall -Wextra
CPP=c++
AFL_CPP=afl-clang++

all: carver

carver: *.cpp *.h
	$(CPP) $(CCFLAGS) -std=c++1y carver.cpp -g -o carver

carver_afl: *.cpp *.h
	$(AFL_CPP) $(CCFLAGS) -std=c++1y carver.cpp -o carver

clean:
	-rm -f carver

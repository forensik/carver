#pragma once

/*
        // Legacy description. The T_* field was the ID. But now its better solved in a enum
		T|S|L_name_HEAD|FOOT_number
		T: tokenID for internal use only
		S: char[]
		L: length of char[], needed for avoiding null-termination
*/

#define S_JPG_HEAD_1	"\xff\xd8\xff\xe0\x00\x10"
#define L_JPG_HEAD_1	6

#define S_JPG_HEAD_2	"\xff\xd8\xff\xe1"
#define L_JPG_HEAD_2	4

#define S_JPG_HEAD_3	"\xff\xd8\xff"
#define L_JPG_HEAD_3	3

#define S_JPG_FOOT		"\xff\xd9"
#define L_JPG_FOOT		2

#define S_GIF_HEAD_1	"\x47\x49\x46\x38\x37\x61"
#define L_GIF_HEAD_1	6

#define S_GIF_HEAD_2	"\x47\x49\x46\x38\x39\x61"
#define L_GIF_HEAD_2	6

#define S_GIF_FOOT_1	"\x00\x3b"
#define L_GIF_FOOT_1	2

//#define S_GIF_FOOT_2	"\x00\x00\x3b"
//#define L_GIF_FOOT_2	3

#define S_PNG_HEAD		"\x89\x50\x4e\x47"	
#define L_PNG_HEAD		4

//#define S_PNG_FOOT		"\xff\xfc\xfd\xfe"
//#define L_PNG_FOOT		4

#define S_BMP_HEAD		"BM"
#define L_BMP_HEAD		2

#define S_DOC_HEAD		"\xd0\xcf\x11\xe0\xa1\xb1"
#define L_DOC_HEAD		6

#define S_PDF_HEAD		"%PDF-"
#define L_PDF_HEAD		5

#define S_PDF_FOOT		"%%EOF"
#define L_PDF_FOOT		5

enum IDS{
    T_JPG_HEAD_1,
    T_JPG_HEAD_2,
    T_JPG_HEAD_3,
    T_JPG_FOOT,
    T_GIF_HEAD_1,
    T_GIF_HEAD_2,
    T_GIF_FOOT_1,
    T_GIF_FOOT_2, // unused
    T_PNG_HEAD,
    T_PNG_FOOT, // unused
    T_BMP_HEAD,
    T_DOC_HEAD,
    T_PDF_HEAD,
    T_PDF_FOOT
};


const char* debug_print_token(int id){
    switch (id){
		case 0 : return "JPG_HEAD_1";	
		case 1 : return "JPG_HEAD_2";
		case 2 : return "JPG_HEAD_3";
		case 3 : return "JPG_FOOT";	
		case 4 : return "GIF_HEAD_1";	
		case 5 : return "GIF_HEAD_2";	
		case 6 : return "GIF_FOOT_1";	
		case 7 : return "GIF_FOOT_2";	
		case 8 : return "PNG_HEAD";	
		case 9 : return "PNG_FOOT";	
		case 10: return "BMP_HEAD";	
		case 11: return "DOC_HEAD";	
		case 12: return "PDF_HEAD";	
		case 13: return "PDF_FOOT";	

        default: return "token error";

    }
}


Introduction
-------------------

In the forensic workflow with harddrive images a carver is a common tool
for gathering deleted data. This kind of programm searches for common
filebeginnings and endings and stores the data inbetween to the
harddrive. A common problem arising with that approach is, especially
with fragmentated harddrive-images, that lots of these extracted files
are invalid and unreadable.

The programm we provide here is a simple filecarver for
PNG, BMP, PDF, DOC, GIF, JPG but does simple checks to verify the files
if they are valid and so reduce the time the programm-operator has to 
spend on sorting out garbage.

While the programm itself is compact, performant and jet quite modular
to be simple extendible for other fileformats, it is simple and
intuitive to use on the commandline.

In contrast to some other simple carving tools, this programm can find
the mentioned filetypes at each position in the image and not just in
some junk boundaries.


Building and usage
-------------------

Building of the program is done by just typing 'make'.

You should use a current compiler for that, that supports the
c++1y-standard, because there are new c++ features used in the
sourcecode.

Running the programm is also straight forward:
just type:
    ./carver path_to_image/image_name.img

this command will creates a folder named output, and subfolders which
correspond to  the filetypeextensions of the found files


Hacking
-------------------

Hacking and extending the programm:
first of all you should be familiar with c++ and its STL-features like
(forward)lists etc.

Extending the programm with a new fileformat support is also quite
simple:
    1.) add the header- and footer-signature to the headers.h file.
        Because these signatures might conatain the '\0' Character
        you also must provide the length of the signature.
    
    2.) Write the filechecking routine.
        A good reference to look at is the png_handler.h.
        The checking routine has in general 2 parameters:
        The forward list of pairs, which correspond to potential begin-
        and end-positions of the file on the image.
        The second argument is the ifstream of the file-image.
        
        This function itself returns a list of beginnings and endings
        (as pairs) which should be written out to disk.

    3.) Add the calls and output actions to the carver.cpp
        (just look at the other filetypes, in that file and add them
        accordingly.)
    
Hacking the Mainloop.
    One doesn't have to look at this routine, if you simply want to
    support a new fileformat for searching, but we explain the
    function of this, because it is the core of the programm.
    As mention above, this programm cares about finding the signatures
    at _each_ place in the image.
    Because main-memory is limited to several hundred MBs, while the
    images can be several GBs up to TBs.
    To achive finding the signatues, we use a buffer, which is filled in
    an overlapping manner.
    This is quite complicated (due to EOF, finding a Sinature in the
    overlapping area or a part of the overlapping area, etc.) this
    function has its own documentation in searcher.pdf.

for updates etc. look at "gitlab.com/forensik/carver"

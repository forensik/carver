#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include "chararrutil.h"


#define MAX_LEN_TRAILER 128
#define MAX_PDF_LINE 256

//documentaion of pdf-file-format: http://www.adobe.com/content/dam/Adobe/en/devnet/acrobat/pdfs/pdf_reference_1-7.pdf
using namespace std;

bool isValid(char* binTrailer, unsigned long fileSize, unsigned long start, ifstream& image){
	string trailer(binTrailer);

	//finding the offset to the xref-table
	unsigned int endOfXrefOffset = trailer.rfind("\n", MAX_LEN_TRAILER-L_PDF_FOOT-2);
	unsigned int startOFXrefOffset = trailer.rfind("\n", MAX_LEN_TRAILER-L_PDF_FOOT-3);
	
	unsigned long offsetXrefTable = 0;
	try{
		string str_offsetXrefTable(trailer.substr(startOFXrefOffset, endOfXrefOffset-startOFXrefOffset));
		offsetXrefTable = stoi(str_offsetXrefTable);
		//@debug
		//cout << "offsetXref: " << offsetXrefTable << endl;
	}catch(...){return false;}
	if(fileSize <= offsetXrefTable){return false;}

	//parse the xref-table	
	char buf[21];
	buf[20] = '\x00';
	image.seekg(start + offsetXrefTable, image.beg);
	image.getline(buf, 5);
	buf[4] = '\x00';
	if(string(buf).compare("xref") != 0){return false;}
	auto * objects = new forward_list<pair<unsigned long, unsigned int> >();
	
	//xref-table was found, parsing lines
	while(1){
		image.getline(buf, 20);
		string line(buf);
		//end of xref-table ==> stop parsing
		if(line.compare(0, 7, "trailer") == 0){break;}
		unsigned int numObjects = 0;
		unsigned int objID = 0;

		try{
			unsigned int posNumObjects = line.find(" ");
			objID = stoi(line.substr(0, posNumObjects));
			numObjects = stoi(line.substr(posNumObjects, line.length()));
			//@debug
			//cout << "objID: " << objID << " numObjects: " << numObjects << endl;
		}catch(...){return false;}


		for(unsigned int i = 0; i < numObjects; i++){
			unsigned long objOffset = 0;
			try{
				image.getline(buf, 21);
				line = string(buf);
				objOffset = stoi(line.substr(0, 10));
				char type = buf[17];
				//@debug
				//cout << "off: " << objOffset << " gen: " << generation << " type: " << type << endl;
				if(!(type == 'n' || type == 'f')){
					return false;
				}
				if(type == 'n'){
					objects->push_front(pair<unsigned long, unsigned int>(objOffset, objID+i));
				}
			}catch(...){return false;}
		}
	}
	objects->sort();
	image.seekg(start, image.beg);
	char lineBuf[MAX_PDF_LINE+1];
	//check if there are objects, where the xref-table states they should be
	unsigned long lastObjEnd = start;
	for(auto obj : *objects){
		//check every object
		image.seekg(start+obj.first, image.beg);
		bool firstLine = true;
		while(1){
			image.getline(lineBuf, MAX_PDF_LINE);
			string line(lineBuf);
			lastObjEnd += line.length()+1;
			if(line.find("endobj") != string::npos || line.length() == 0){break;}
			if(firstLine){
				firstLine = false;
				string objIdString = line.substr(0, line.find(" "));
				if((unsigned long)stoi(objIdString) != obj.second){return false;}
			}
			if((line.compare("obj") == 0) && (obj.first < lastObjEnd)){return false;}
		}
		
	}

	return true;
}


auto * pdf_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image){
	auto * results = new list< pair<unsigned long, unsigned long> >;
	if(!image){return results;}
	forward_list<pair<unsigned long, int> >* tmp = new forward_list<pair<unsigned long, int> >(*found);

	while(!tmp->empty()){
		auto t = tmp->front();
		tmp->pop_front();
		if((t.second != T_PDF_HEAD)) {continue;}

		for(auto next : *tmp){

			if((next.second != T_PDF_FOOT)){continue;}
			unsigned long fileSize = next.first + L_PDF_FOOT + 1 - t.first;
			char trailer[MAX_LEN_TRAILER];
			image.seekg(t.first + fileSize - MAX_LEN_TRAILER);
			image.read(trailer, MAX_LEN_TRAILER);
			if(isValid(trailer, fileSize, t.first, image)){
				results->push_back({t.first, t.first+fileSize});
			}
			break;
			
		}
		
	}
	image.seekg(0, image.beg);
	image.clear();
	delete tmp;
	return results;
}



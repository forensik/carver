#pragma once
#include <forward_list>
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include "chararrutil.h"
#include <boost/crc.hpp>


#define MAX_PNG_SIZE 3*1024*1024
using namespace std;

struct chunk{
	unsigned int length;
	char type[4];
	char* data;
	unsigned int crc;
};


auto * png_handler(forward_list< pair<unsigned long, int> > * found, ifstream& image){
	auto * results = new list< pair<unsigned long, unsigned long> >;
	results->clear();
	if(!image){return results;}
	

	for(auto t : *found){
		if((t.second != T_PNG_HEAD)) {continue;}
	



		image.seekg(t.first, image.beg);
		char potentialHeader[8];
		image.read(potentialHeader, 8 /*L_PNG_HEAD*/);
		//recheck header
		if(0 != memcmp(potentialHeader, S_PNG_HEAD, L_PNG_HEAD)){
			continue;
		}


		//sking the PNG-header
		image.seekg(t.first + 8 /*L_PNG_HEAD*/, image.beg); // Zeichen (0-7) sind der PNG Header, auch wenn wir weniger davon ueberpruefen
		
		char data[MAX_PNG_SIZE];
		image.read(data, MAX_PNG_SIZE);
		char* currentPosition = data;
		if(NULL == currentPosition){
			continue;
		}

        // Der erste Chunk MUSS der IHDR Chunk sein. dieser liegt somit hier:
        //   PNGHEADER | IHDR-laenge | IHDR als String |
        //    8 Byte      4 Byte        4 Byte
        //             ^
        //             currentPosition

        if (0x49484452 != getuint_2(currentPosition+4, 4)){
            continue;
        }


        int q = 1024; // breche nach 1024 Chunks ab. (Um lange analsephasen zu vermeiden (regulaer sind weniger als 10 Chunks drin))

		while(currentPosition < data + MAX_PNG_SIZE){
            q--;
            if (q == 0){
                break;
            }
			struct chunk cnk;
			if(currentPosition-data+3 >= MAX_PNG_SIZE){
				break;
			}

//        dump_hex(currentPosition, 8);
			cnk.length = getuint_2(currentPosition, 4);

			currentPosition += 4;
			if(currentPosition-data+3 >= MAX_PNG_SIZE){
				break;
			}
			
			cnk.type[0] = currentPosition++[0];
			cnk.type[1] = currentPosition++[0];
			cnk.type[2] = currentPosition++[0];
			cnk.type[3] = currentPosition++[0];
			//cnk.type[4] = '\x00';
			
			//@debug
			//cout << "type: " << cnk.type << endl;

			cnk.data = currentPosition;
			currentPosition += cnk.length;
			if(currentPosition-data+3 >= MAX_PNG_SIZE){
				//@debug
				//cout << "cnk.length: " << cnk.length << endl;
				break;
			}
			cnk.crc = getuint_2(currentPosition, 4);

			//sadly declaring the chunk sturct with "__attribute__((packed))" doesn't seem to work :(
			char buf_crc[4+cnk.length];
			buf_crc[0] = cnk.type[0];
			buf_crc[1] = cnk.type[1];
			buf_crc[2] = cnk.type[2];
			buf_crc[3] = cnk.type[3];
			for(unsigned int i = 0; i < cnk.length; i++) buf_crc[i+4] = cnk.data[i];


			//@debug
			//cout << "cnk.crc: " << hex << cnk.crc << endl << "boost::crc: " <<
			//	~boost::crc<32, 0x04C11DB7, 0xFFFFFFFF, 0, true, true>(buf_crc, cnk.length+4) << endl;

			if(~boost::crc<32, 0x04C11DB7, 0xFFFFFFFF, 0, true, true>(buf_crc, cnk.length+4) != cnk.crc){
				break;
			}

			currentPosition += 4;
			if(strncmp(cnk.type, "IEND", 4) == 0 && (cnk.length == 0)){
					results->push_back( {t.first, t.first + 8 + currentPosition - data});
					break;
			}
		}
	}
	image.seekg(0, image.beg);
	image.clear();
	return results;
}


